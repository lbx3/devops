package apis

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/go-admin-team/go-admin-core/sdk/api"
	"github.com/go-admin-team/go-admin-core/sdk/pkg/jwtauth/user"
	_ "github.com/go-admin-team/go-admin-core/sdk/pkg/response"

	"go-admin/app/admin/models"
	"go-admin/app/admin/service"
	"go-admin/app/admin/service/dto"
	"go-admin/common/actions"
)

type DevopsProject struct {
	api.Api
}

// GetPage 获取DevopsProject列表
// @Summary 获取DevopsProject列表
// @Description 获取DevopsProject列表
// @Tags DevopsProject
// @Param projectId query int false "项目ID"
// @Param projectName query string false "项目名"
// @Param gitGroup query string false "GIT组"
// @Param harborProject query string false "制品库"
// @Param deptId query string false "部门ID"
// @Param owner query string false "项目负责人"
// @Param jiraStory query string false "JIRA故事"
// @Param desc query string false "项目描述"
// @Param alias query string false "别名"
// @Param pageSize query int false "页条数"
// @Param pageIndex query int false "页码"
// @Success 200 {object} response.Response{data=response.Page{list=[]models.DevopsProject}} "{"code": 200, "data": [...]}"
// @Router /api/v1/devops-project [get]
// @Security Bearer
func (e DevopsProject) GetPage(c *gin.Context) {
	req := dto.DevopsProjectGetPageReq{}
	s := service.DevopsProject{}
	err := e.MakeContext(c).
		MakeOrm().
		Bind(&req).
		MakeService(&s.Service).
		Errors
	if err != nil {
		e.Logger.Error(err)
		e.Error(500, err, err.Error())
		return
	}

	p := actions.GetPermissionFromContext(c)
	list := make([]models.DevopsProject, 0)
	var count int64

	err = s.GetPage(&req, p, &list, &count)
	if err != nil {
		e.Error(500, err, fmt.Sprintf("获取DevopsProject失败，\r\n失败信息 %s", err.Error()))
		return
	}

	e.PageOK(list, int(count), req.GetPageIndex(), req.GetPageSize(), "查询成功")
}

// Get 获取DevopsProject
// @Summary 获取DevopsProject
// @Description 获取DevopsProject
// @Tags DevopsProject
// @Param id path int false "id"
// @Success 200 {object} response.Response{data=models.DevopsProject} "{"code": 200, "data": [...]}"
// @Router /api/v1/devops-project/{id} [get]
// @Security Bearer
func (e DevopsProject) Get(c *gin.Context) {
	req := dto.DevopsProjectGetReq{}
	s := service.DevopsProject{}
	err := e.MakeContext(c).
		MakeOrm().
		Bind(&req).
		MakeService(&s.Service).
		Errors
	if err != nil {
		e.Logger.Error(err)
		e.Error(500, err, err.Error())
		return
	}
	var object models.DevopsProject

	p := actions.GetPermissionFromContext(c)
	err = s.Get(&req, p, &object)
	if err != nil {
		e.Error(500, err, fmt.Sprintf("获取DevopsProject失败，\r\n失败信息 %s", err.Error()))
		return
	}

	e.OK(object, "查询成功")
}

// Insert 创建DevopsProject
// @Summary 创建DevopsProject
// @Description 创建DevopsProject
// @Tags DevopsProject
// @Accept application/json
// @Product application/json
// @Param data body dto.DevopsProjectInsertReq true "data"
// @Success 200 {object} response.Response	"{"code": 200, "message": "添加成功"}"
// @Router /api/v1/devops-project [post]
// @Security Bearer
func (e DevopsProject) Insert(c *gin.Context) {
	req := dto.DevopsProjectInsertReq{}
	s := service.DevopsProject{}
	err := e.MakeContext(c).
		MakeOrm().
		Bind(&req).
		MakeService(&s.Service).
		Errors
	if err != nil {
		e.Logger.Error(err)
		e.Error(500, err, err.Error())
		return
	}
	// 设置创建人
	req.SetCreateBy(user.GetUserId(c))

	err = s.Insert(&req)
	if err != nil {
		e.Error(500, err, fmt.Sprintf("创建DevopsProject失败，\r\n失败信息 %s", err.Error()))
		return
	}

	e.OK(req.GetId(), "创建成功")
}

// Update 修改DevopsProject
// @Summary 修改DevopsProject
// @Description 修改DevopsProject
// @Tags DevopsProject
// @Accept application/json
// @Product application/json
// @Param id path int true "id"
// @Param data body dto.DevopsProjectUpdateReq true "body"
// @Success 200 {object} response.Response	"{"code": 200, "message": "修改成功"}"
// @Router /api/v1/devops-project/{id} [put]
// @Security Bearer
func (e DevopsProject) Update(c *gin.Context) {
	req := dto.DevopsProjectUpdateReq{}
	s := service.DevopsProject{}
	err := e.MakeContext(c).
		MakeOrm().
		Bind(&req).
		MakeService(&s.Service).
		Errors
	if err != nil {
		e.Logger.Error(err)
		e.Error(500, err, err.Error())
		return
	}
	req.SetUpdateBy(user.GetUserId(c))
	p := actions.GetPermissionFromContext(c)

	err = s.Update(&req, p)
	if err != nil {
		e.Error(500, err, fmt.Sprintf("修改DevopsProject失败，\r\n失败信息 %s", err.Error()))
		return
	}
	e.OK(req.GetId(), "修改成功")
}

// Delete 删除DevopsProject
// @Summary 删除DevopsProject
// @Description 删除DevopsProject
// @Tags DevopsProject
// @Param data body dto.DevopsProjectDeleteReq true "body"
// @Success 200 {object} response.Response	"{"code": 200, "message": "删除成功"}"
// @Router /api/v1/devops-project [delete]
// @Security Bearer
func (e DevopsProject) Delete(c *gin.Context) {
	s := service.DevopsProject{}
	req := dto.DevopsProjectDeleteReq{}
	err := e.MakeContext(c).
		MakeOrm().
		Bind(&req).
		MakeService(&s.Service).
		Errors
	if err != nil {
		e.Logger.Error(err)
		e.Error(500, err, err.Error())
		return
	}

	// req.SetUpdateBy(user.GetUserId(c))
	p := actions.GetPermissionFromContext(c)

	err = s.Remove(&req, p)
	if err != nil {
		e.Error(500, err, fmt.Sprintf("删除DevopsProject失败，\r\n失败信息 %s", err.Error()))
		return
	}
	e.OK(req.GetId(), "删除成功")
}
