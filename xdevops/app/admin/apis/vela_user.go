package apis

import (
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/go-admin-team/go-admin-core/sdk/api"
	"io/ioutil"
	"net/http"
	"time"
)

type VelaUser struct {
	api.Api
}
type Login struct {
	User         User   `json:"user"`
	AccessToken  string `json:"accessToken"`
	RefreshToken string `json:"refreshToken"`
}
type User struct {
	CreateTime    time.Time `json:"createTime"`
	LastLoginTime time.Time `json:"lastLoginTime"`
	Name          string    `json:"name"`
	Email         string    `json:"email"`
	Disabled      bool      `json:"disabled"`
}

func (v *VelaUser) VelaLogin(c *gin.Context) {
	v.MakeContext(c)
	url := "https://vela.lbxdrugs.com/api/v1/auth/login"
	data := []byte(`{"username":"xupengjun","password":"liwanli1985"}`)
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Add("Content-Type", "application/json")
	client := &http.Client{Timeout: time.Second * 10}
	res, _ := client.Do(req)

	defer res.Body.Close()

	body, _ := ioutil.ReadAll(res.Body)

	login := Login{}
	if err := json.Unmarshal(body, &login); err != nil {
		v.Error(500, err, "反序列化失败")
	}
	if res.StatusCode == 200 {
		v.OK(login, "ok")
	}
}
