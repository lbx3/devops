package apis

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/go-admin-team/go-admin-core/sdk/api"
	"github.com/go-admin-team/go-admin-core/sdk/pkg/jwtauth/user"
	_ "github.com/go-admin-team/go-admin-core/sdk/pkg/response"

	"go-admin/app/admin/models"
	"go-admin/app/admin/service"
	"go-admin/app/admin/service/dto"
	"go-admin/common/actions"
)

type DevopsApp struct {
	api.Api
}

// GetPage 获取DevopsApp列表
// @Summary 获取DevopsApp列表
// @Description 获取DevopsApp列表
// @Tags DevopsApp
// @Param appId query int false "应用ID"
// @Param appName query string false "应用名"
// @Param harborImage query string false "制品"
// @Param gitRepo query string false "GIT仓库"
// @Param projectId query string false "项目ID"
// @Param alias query string false "别名"
// @Param desc query string false "描述"
// @Param component query string false "Vela组件"
// @Param env query string false "环境"
// @Param exposeType query string false "暴露方式"
// @Param pageSize query int false "页条数"
// @Param pageIndex query int false "页码"
// @Success 200 {object} response.Response{data=response.Page{list=[]models.DevopsApp}} "{"code": 200, "data": [...]}"
// @Router /api/v1/devops-app [get]
// @Security Bearer
func (e DevopsApp) GetPage(c *gin.Context) {
	req := dto.DevopsAppGetPageReq{}
	s := service.DevopsApp{}
	err := e.MakeContext(c).
		MakeOrm().
		Bind(&req).
		MakeService(&s.Service).
		Errors
	if err != nil {
		e.Logger.Error(err)
		e.Error(500, err, err.Error())
		return
	}

	p := actions.GetPermissionFromContext(c)
	list := make([]models.DevopsApp, 0)
	var count int64

	err = s.GetPage(&req, p, &list, &count)
	if err != nil {
		e.Error(500, err, fmt.Sprintf("获取DevopsApp失败，\r\n失败信息 %s", err.Error()))
		return
	}

	e.PageOK(list, int(count), req.GetPageIndex(), req.GetPageSize(), "查询成功")
}

// Get 获取DevopsApp
// @Summary 获取DevopsApp
// @Description 获取DevopsApp
// @Tags DevopsApp
// @Param id path int false "id"
// @Success 200 {object} response.Response{data=models.DevopsApp} "{"code": 200, "data": [...]}"
// @Router /api/v1/devops-app/{id} [get]
// @Security Bearer
func (e DevopsApp) Get(c *gin.Context) {
	req := dto.DevopsAppGetReq{}
	s := service.DevopsApp{}
	err := e.MakeContext(c).
		MakeOrm().
		Bind(&req).
		MakeService(&s.Service).
		Errors
	if err != nil {
		e.Logger.Error(err)
		e.Error(500, err, err.Error())
		return
	}
	var object models.DevopsApp

	p := actions.GetPermissionFromContext(c)
	err = s.Get(&req, p, &object)
	if err != nil {
		e.Error(500, err, fmt.Sprintf("获取DevopsApp失败，\r\n失败信息 %s", err.Error()))
		return
	}

	e.OK(object, "查询成功")
}

// Insert 创建DevopsApp
// @Summary 创建DevopsApp
// @Description 创建DevopsApp
// @Tags DevopsApp
// @Accept application/json
// @Product application/json
// @Param data body dto.DevopsAppInsertReq true "data"
// @Success 200 {object} response.Response	"{"code": 200, "message": "添加成功"}"
// @Router /api/v1/devops-app [post]
// @Security Bearer
func (e DevopsApp) Insert(c *gin.Context) {
	req := dto.DevopsAppInsertReq{}
	s := service.DevopsApp{}
	err := e.MakeContext(c).
		MakeOrm().
		Bind(&req).
		MakeService(&s.Service).
		Errors
	if err != nil {
		e.Logger.Error(err)
		e.Error(500, err, err.Error())
		return
	}
	// 设置创建人
	req.SetCreateBy(user.GetUserId(c))

	err = s.Insert(&req)
	if err != nil {
		e.Error(500, err, fmt.Sprintf("创建DevopsApp失败，\r\n失败信息 %s", err.Error()))
		return
	}

	e.OK(req.GetId(), "创建成功")
}

// Update 修改DevopsApp
// @Summary 修改DevopsApp
// @Description 修改DevopsApp
// @Tags DevopsApp
// @Accept application/json
// @Product application/json
// @Param id path int true "id"
// @Param data body dto.DevopsAppUpdateReq true "body"
// @Success 200 {object} response.Response	"{"code": 200, "message": "修改成功"}"
// @Router /api/v1/devops-app/{id} [put]
// @Security Bearer
func (e DevopsApp) Update(c *gin.Context) {
	req := dto.DevopsAppUpdateReq{}
	s := service.DevopsApp{}
	err := e.MakeContext(c).
		MakeOrm().
		Bind(&req).
		MakeService(&s.Service).
		Errors
	if err != nil {
		e.Logger.Error(err)
		e.Error(500, err, err.Error())
		return
	}
	req.SetUpdateBy(user.GetUserId(c))
	p := actions.GetPermissionFromContext(c)

	err = s.Update(&req, p)
	if err != nil {
		e.Error(500, err, fmt.Sprintf("修改DevopsApp失败，\r\n失败信息 %s", err.Error()))
		return
	}
	e.OK(req.GetId(), "修改成功")
}

// Delete 删除DevopsApp
// @Summary 删除DevopsApp
// @Description 删除DevopsApp
// @Tags DevopsApp
// @Param data body dto.DevopsAppDeleteReq true "body"
// @Success 200 {object} response.Response	"{"code": 200, "message": "删除成功"}"
// @Router /api/v1/devops-app [delete]
// @Security Bearer
func (e DevopsApp) Delete(c *gin.Context) {
	s := service.DevopsApp{}
	req := dto.DevopsAppDeleteReq{}
	err := e.MakeContext(c).
		MakeOrm().
		Bind(&req).
		MakeService(&s.Service).
		Errors
	if err != nil {
		e.Logger.Error(err)
		e.Error(500, err, err.Error())
		return
	}

	// req.SetUpdateBy(user.GetUserId(c))
	p := actions.GetPermissionFromContext(c)

	err = s.Remove(&req, p)
	if err != nil {
		e.Error(500, err, fmt.Sprintf("删除DevopsApp失败，\r\n失败信息 %s", err.Error()))
		return
	}
	e.OK(req.GetId(), "删除成功")
}
