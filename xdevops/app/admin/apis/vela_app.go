package apis

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-admin-team/go-admin-core/sdk/api"
	_ "github.com/go-admin-team/go-admin-core/sdk/pkg/response"
	"go-admin/app/admin/models"
	"io/ioutil"
	"net/http"
	"time"
)

type VelaApp struct {
	api.Api
}

func (v *VelaApp) GetPage(c *gin.Context) {
	v.MakeContext(c)
	url := "https://vela.lbxdrugs.com/api/v1/applications"
	token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Inh1cGVuZ2p1biIsImdyYW50VHlwZSI6ImFjY2VzcyIsImV4cCI6MTY4NjgwMTI2NywiaXNzIjoidmVsYS1pc3N1ZXIiLCJuYmYiOjE2ODY3OTc2Njd9.5Trn0cmcFJb8kxMIAmvtQLPVinXZW2hKD2Us9i77aqM"
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
	client := &http.Client{Timeout: time.Second * 10}
	res, err := client.Do(req)
	if err != nil {
		v.Error(500, err, "请求失败")
	} else if res.StatusCode == 401 {
		v.Error(401, err, "认证失败.")
	} else if res.StatusCode != 200 {
		v.Error(500, err, "认证失败.")
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		v.Error(500, err, "读取响应内容失败")
		return
	}

	resp := models.JSONData{}
	if err := json.Unmarshal(body, &resp); err != nil {
		v.Error(500, err, "反序列化失败")
		return
	}
	if res.StatusCode == 200 {
		v.OK(resp, "ok")
	}
}
