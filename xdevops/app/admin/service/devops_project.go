package service

import (
	"errors"

	"github.com/go-admin-team/go-admin-core/sdk/service"
	"gorm.io/gorm"

	"go-admin/app/admin/models"
	"go-admin/app/admin/service/dto"
	"go-admin/common/actions"
	cDto "go-admin/common/dto"
)

type DevopsProject struct {
	service.Service
}

// GetPage 获取DevopsProject列表
func (e *DevopsProject) GetPage(c *dto.DevopsProjectGetPageReq, p *actions.DataPermission, list *[]models.DevopsProject, count *int64) error {
	var err error
	var data models.DevopsProject

	err = e.Orm.Model(&data).
		Scopes(
			cDto.MakeCondition(c.GetNeedSearch()),
			cDto.Paginate(c.GetPageSize(), c.GetPageIndex()),
			actions.Permission(data.TableName(), p),
		).
		Find(list).Limit(-1).Offset(-1).
		Count(count).Error
	if err != nil {
		e.Log.Errorf("DevopsProjectService GetPage error:%s \r\n", err)
		return err
	}
	return nil
}

// Get 获取DevopsProject对象
func (e *DevopsProject) Get(d *dto.DevopsProjectGetReq, p *actions.DataPermission, model *models.DevopsProject) error {
	var data models.DevopsProject

	err := e.Orm.Model(&data).
		Scopes(
			actions.Permission(data.TableName(), p),
		).
		First(model, d.GetId()).Error
	if err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		err = errors.New("查看对象不存在或无权查看")
		e.Log.Errorf("Service GetDevopsProject error:%s \r\n", err)
		return err
	}
	if err != nil {
		e.Log.Errorf("db error:%s", err)
		return err
	}
	return nil
}

// Insert 创建DevopsProject对象
func (e *DevopsProject) Insert(c *dto.DevopsProjectInsertReq) error {
	var err error
	var data models.DevopsProject
	c.Generate(&data)
	err = e.Orm.Create(&data).Error
	if err != nil {
		e.Log.Errorf("DevopsProjectService Insert error:%s \r\n", err)
		return err
	}
	return nil
}

// Update 修改DevopsProject对象
func (e *DevopsProject) Update(c *dto.DevopsProjectUpdateReq, p *actions.DataPermission) error {
	var err error
	var data = models.DevopsProject{}
	e.Orm.Scopes(
		actions.Permission(data.TableName(), p),
	).First(&data, c.GetId())
	c.Generate(&data)

	db := e.Orm.Save(&data)
	if err = db.Error; err != nil {
		e.Log.Errorf("DevopsProjectService Save error:%s \r\n", err)
		return err
	}
	if db.RowsAffected == 0 {
		return errors.New("无权更新该数据")
	}
	return nil
}

// Remove 删除DevopsProject
func (e *DevopsProject) Remove(d *dto.DevopsProjectDeleteReq, p *actions.DataPermission) error {
	var data models.DevopsProject

	db := e.Orm.Model(&data).
		Scopes(
			actions.Permission(data.TableName(), p),
		).Delete(&data, d.GetId())
	if err := db.Error; err != nil {
		e.Log.Errorf("Service RemoveDevopsProject error:%s \r\n", err)
		return err
	}
	if db.RowsAffected == 0 {
		return errors.New("无权删除该数据")
	}
	return nil
}
