package dto

import (
	"go-admin/app/admin/models"
	"go-admin/common/dto"
	common "go-admin/common/models"
)

type DevopsAppGetPageReq struct {
	dto.Pagination `search:"-"`
	AppId          int    `form:"appId"  search:"type:exact;column:app_id;table:devops_app" comment:"应用ID"`
	AppName        string `form:"appName"  search:"type:exact;column:app_name;table:devops_app" comment:"应用名"`
	HarborImage    string `form:"harborImage"  search:"type:exact;column:harbor_image;table:devops_app" comment:"制品"`
	GitRepo        string `form:"gitRepo"  search:"type:exact;column:git_repo;table:devops_app" comment:"GIT仓库"`
	ProjectId      string `form:"projectId"  search:"type:exact;column:project_id;table:devops_app" comment:"项目ID"`
	Alias          string `form:"alias"  search:"type:exact;column:alias;table:devops_app" comment:"别名"`
	Desc           string `form:"desc"  search:"type:exact;column:desc;table:devops_app" comment:"描述"`
	Component      string `form:"component"  search:"type:exact;column:component;table:devops_app" comment:"Vela组件"`
	Env            string `form:"env"  search:"type:exact;column:env;table:devops_app" comment:"环境"`
	ExposeType     string `form:"exposeType"  search:"type:exact;column:expose_type;table:devops_app" comment:"暴露方式"`
	DevopsAppOrder
}

type DevopsAppOrder struct {
	AppId       string `form:"appIdOrder"  search:"type:order;column:app_id;table:devops_app"`
	AppName     string `form:"appNameOrder"  search:"type:order;column:app_name;table:devops_app"`
	CreateBy    string `form:"createByOrder"  search:"type:order;column:create_by;table:devops_app"`
	UpdateBy    string `form:"updateByOrder"  search:"type:order;column:update_by;table:devops_app"`
	CreatedAt   string `form:"createdAtOrder"  search:"type:order;column:created_at;table:devops_app"`
	UpdatedAt   string `form:"updatedAtOrder"  search:"type:order;column:updated_at;table:devops_app"`
	DeletedAt   string `form:"deletedAtOrder"  search:"type:order;column:deleted_at;table:devops_app"`
	HarborImage string `form:"harborImageOrder"  search:"type:order;column:harbor_image;table:devops_app"`
	GitRepo     string `form:"gitRepoOrder"  search:"type:order;column:git_repo;table:devops_app"`
	ProjectId   string `form:"projectIdOrder"  search:"type:order;column:project_id;table:devops_app"`
	Alias       string `form:"aliasOrder"  search:"type:order;column:alias;table:devops_app"`
	Desc        string `form:"descOrder"  search:"type:order;column:desc;table:devops_app"`
	Component   string `form:"componentOrder"  search:"type:order;column:component;table:devops_app"`
	Env         string `form:"envOrder"  search:"type:order;column:env;table:devops_app"`
	ExposeType  string `form:"exposeTypeOrder"  search:"type:order;column:expose_type;table:devops_app"`
}

func (m *DevopsAppGetPageReq) GetNeedSearch() interface{} {
	return *m
}

type DevopsAppInsertReq struct {
	AppId       int    `json:"-" comment:"应用ID"` // 应用ID
	AppName     string `json:"appName" comment:"应用名"`
	HarborImage string `json:"harborImage" comment:"制品"`
	GitRepo     string `json:"gitRepo" comment:"GIT仓库"`
	ProjectId   string `json:"projectId" comment:"项目ID"`
	Alias       string `json:"alias" comment:"别名"`
	Desc        string `json:"desc" comment:"描述"`
	Component   string `json:"component" comment:"Vela组件"`
	Env         string `json:"env" comment:"环境"`
	ExposeType  string `json:"exposeType" comment:"暴露方式"`
	common.ControlBy
}

func (s *DevopsAppInsertReq) Generate(model *models.DevopsApp) {
	if s.AppId == 0 {
		model.Model = common.Model{Id: s.AppId}
	}
	model.AppName = s.AppName
	model.CreateBy = s.CreateBy // 添加这而，需要记录是被谁创建的
	model.HarborImage = s.HarborImage
	model.GitRepo = s.GitRepo
	model.ProjectId = s.ProjectId
	model.Alias = s.Alias
	model.Desc = s.Desc
	model.Component = s.Component
	model.Env = s.Env
	model.ExposeType = s.ExposeType
}

func (s *DevopsAppInsertReq) GetId() interface{} {
	return s.AppId
}

type DevopsAppUpdateReq struct {
	AppId       int    `uri:"appId" comment:"应用ID"` // 应用ID
	AppName     string `json:"appName" comment:"应用名"`
	HarborImage string `json:"harborImage" comment:"制品"`
	GitRepo     string `json:"gitRepo" comment:"GIT仓库"`
	ProjectId   string `json:"projectId" comment:"项目ID"`
	Alias       string `json:"alias" comment:"别名"`
	Desc        string `json:"desc" comment:"描述"`
	Component   string `json:"component" comment:"Vela组件"`
	Env         string `json:"env" comment:"环境"`
	ExposeType  string `json:"exposeType" comment:"暴露方式"`
	common.ControlBy
}

func (s *DevopsAppUpdateReq) Generate(model *models.DevopsApp) {
	if s.AppId == 0 {
		model.Model = common.Model{Id: s.AppId}
	}
	model.AppName = s.AppName
	model.UpdateBy = s.UpdateBy // 添加这而，需要记录是被谁更新的
	model.HarborImage = s.HarborImage
	model.GitRepo = s.GitRepo
	model.ProjectId = s.ProjectId
	model.Alias = s.Alias
	model.Desc = s.Desc
	model.Component = s.Component
	model.Env = s.Env
	model.ExposeType = s.ExposeType
}

func (s *DevopsAppUpdateReq) GetId() interface{} {
	return s.AppId
}

// DevopsAppGetReq 功能获取请求参数
type DevopsAppGetReq struct {
	AppId int `uri:"appId"`
}

func (s *DevopsAppGetReq) GetId() interface{} {
	return s.AppId
}

// DevopsAppDeleteReq 功能删除请求参数
type DevopsAppDeleteReq struct {
	Ids []int `json:"ids"`
}

func (s *DevopsAppDeleteReq) GetId() interface{} {
	return s.Ids
}
