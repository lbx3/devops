package dto

import (
	_ "time"

	"go-admin/app/admin/models"
	"go-admin/common/dto"
	common "go-admin/common/models"
)

type DevopsProjectGetPageReq struct {
	dto.Pagination `search:"-"`
	ProjectId      int    `form:"projectId"  search:"type:exact;column:project_id;table:devops_project" comment:"项目ID"`
	ProjectName    string `form:"projectName"  search:"type:exact;column:project_name;table:devops_project" comment:"项目名"`
	GitGroup       string `form:"gitGroup"  search:"type:exact;column:git_group;table:devops_project" comment:"GIT组"`
	HarborProject  string `form:"harborProject"  search:"type:exact;column:harbor_project;table:devops_project" comment:"制品库"`
	DeptId         string `form:"deptId"  search:"type:exact;column:dept_id;table:devops_project" comment:"部门ID"`
	Owner          string `form:"owner"  search:"type:exact;column:owner;table:devops_project" comment:"项目负责人"`
	JiraStory      string `form:"jiraStory"  search:"type:exact;column:jira_story;table:devops_project" comment:"JIRA故事"`
	Desc           string `form:"desc"  search:"type:exact;column:desc;table:devops_project" comment:"项目描述"`
	Alias          string `form:"alias"  search:"type:exact;column:alias;table:devops_project" comment:"别名"`
	DevopsProjectOrder
}

type DevopsProjectOrder struct {
	ProjectId     string `form:"projectIdOrder"  search:"type:order;column:project_id;table:devops_project"`
	ProjectName   string `form:"projectNameOrder"  search:"type:order;column:project_name;table:devops_project"`
	CreateBy      string `form:"createByOrder"  search:"type:order;column:create_by;table:devops_project"`
	UpdateBy      string `form:"updateByOrder"  search:"type:order;column:update_by;table:devops_project"`
	CreatedAt     string `form:"createdAtOrder"  search:"type:order;column:created_at;table:devops_project"`
	UpdatedAt     string `form:"updatedAtOrder"  search:"type:order;column:updated_at;table:devops_project"`
	DeletedAt     string `form:"deletedAtOrder"  search:"type:order;column:deleted_at;table:devops_project"`
	GitGroup      string `form:"gitGroupOrder"  search:"type:order;column:git_group;table:devops_project"`
	HarborProject string `form:"harborProjectOrder"  search:"type:order;column:harbor_project;table:devops_project"`
	DeptId        string `form:"deptIdOrder"  search:"type:order;column:dept_id;table:devops_project"`
	Owner         string `form:"ownerOrder"  search:"type:order;column:owner;table:devops_project"`
	JiraStory     string `form:"jiraStoryOrder"  search:"type:order;column:jira_story;table:devops_project"`
	Desc          string `form:"descOrder"  search:"type:order;column:desc;table:devops_project"`
	Alias         string `form:"aliasOrder"  search:"type:order;column:alias;table:devops_project"`
}

func (m *DevopsProjectGetPageReq) GetNeedSearch() interface{} {
	return *m
}

type DevopsProjectInsertReq struct {
	ProjectId     int    `json:"-" comment:"项目ID"` // 项目ID
	ProjectName   string `json:"projectName" comment:"项目名"`
	GitGroup      string `json:"gitGroup" comment:"GIT组"`
	HarborProject string `json:"harborProject" comment:"制品库"`
	DeptId        string `json:"deptId" comment:"部门ID"`
	Owner         string `json:"owner" comment:"项目负责人"`
	JiraStory     string `json:"jiraStory" comment:"JIRA故事"`
	Desc          string `json:"desc" comment:"项目描述"`
	Alias         string `json:"alias" comment:"别名"`
	common.ControlBy
}

func (s *DevopsProjectInsertReq) Generate(model *models.DevopsProject) {
	if s.ProjectId == 0 {
		model.Model = common.Model{Id: s.ProjectId}
	}
	model.ProjectName = s.ProjectName
	model.CreateBy = s.CreateBy // 添加这而，需要记录是被谁创建的
	model.GitGroup = s.GitGroup
	model.HarborProject = s.HarborProject
	model.DeptId = s.DeptId
	model.Owner = s.Owner
	model.JiraStory = s.JiraStory
	model.Desc = s.Desc
	model.Alias = s.Alias
}

func (s *DevopsProjectInsertReq) GetId() interface{} {
	return s.ProjectId
}

type DevopsProjectUpdateReq struct {
	ProjectId     int    `uri:"projectId" comment:"项目ID"` // 项目ID
	ProjectName   string `json:"projectName" comment:"项目名"`
	GitGroup      string `json:"gitGroup" comment:"GIT组"`
	HarborProject string `json:"harborProject" comment:"制品库"`
	DeptId        string `json:"deptId" comment:"部门ID"`
	Owner         string `json:"owner" comment:"项目负责人"`
	JiraStory     string `json:"jiraStory" comment:"JIRA故事"`
	Desc          string `json:"desc" comment:"项目描述"`
	Alias         string `json:"alias" comment:"别名"`
	common.ControlBy
}

func (s *DevopsProjectUpdateReq) Generate(model *models.DevopsProject) {
	if s.ProjectId == 0 {
		model.Model = common.Model{Id: s.ProjectId}
	}
	model.ProjectName = s.ProjectName
	model.UpdateBy = s.UpdateBy // 添加这而，需要记录是被谁更新的
	model.GitGroup = s.GitGroup
	model.HarborProject = s.HarborProject
	model.DeptId = s.DeptId
	model.Owner = s.Owner
	model.JiraStory = s.JiraStory
	model.Desc = s.Desc
	model.Alias = s.Alias
}

func (s *DevopsProjectUpdateReq) GetId() interface{} {
	return s.ProjectId
}

// DevopsProjectGetReq 功能获取请求参数
type DevopsProjectGetReq struct {
	ProjectId int `uri:"projectId"`
}

func (s *DevopsProjectGetReq) GetId() interface{} {
	return s.ProjectId
}

// DevopsProjectDeleteReq 功能删除请求参数
type DevopsProjectDeleteReq struct {
	Ids []int `json:"ids"`
}

func (s *DevopsProjectDeleteReq) GetId() interface{} {
	return s.Ids
}
