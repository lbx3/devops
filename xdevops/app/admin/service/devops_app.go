package service

import (
	"errors"

	"github.com/go-admin-team/go-admin-core/sdk/service"
	"gorm.io/gorm"

	"go-admin/app/admin/models"
	"go-admin/app/admin/service/dto"
	"go-admin/common/actions"
	cDto "go-admin/common/dto"
)

type DevopsApp struct {
	service.Service
}

// GetPage 获取DevopsApp列表
func (e *DevopsApp) GetPage(c *dto.DevopsAppGetPageReq, p *actions.DataPermission, list *[]models.DevopsApp, count *int64) error {
	var err error
	var data models.DevopsApp

	err = e.Orm.Model(&data).
		Scopes(
			cDto.MakeCondition(c.GetNeedSearch()),
			cDto.Paginate(c.GetPageSize(), c.GetPageIndex()),
			actions.Permission(data.TableName(), p),
		).
		Find(list).Limit(-1).Offset(-1).
		Count(count).Error
	if err != nil {
		e.Log.Errorf("DevopsAppService GetPage error:%s \r\n", err)
		return err
	}
	return nil
}

// Get 获取DevopsApp对象
func (e *DevopsApp) Get(d *dto.DevopsAppGetReq, p *actions.DataPermission, model *models.DevopsApp) error {
	var data models.DevopsApp

	err := e.Orm.Model(&data).
		Scopes(
			actions.Permission(data.TableName(), p),
		).
		First(model, d.GetId()).Error
	if err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		err = errors.New("查看对象不存在或无权查看")
		e.Log.Errorf("Service GetDevopsApp error:%s \r\n", err)
		return err
	}
	if err != nil {
		e.Log.Errorf("db error:%s", err)
		return err
	}
	return nil
}

// Insert 创建DevopsApp对象
func (e *DevopsApp) Insert(c *dto.DevopsAppInsertReq) error {
	var err error
	var data models.DevopsApp
	c.Generate(&data)
	err = e.Orm.Create(&data).Error
	if err != nil {
		e.Log.Errorf("DevopsAppService Insert error:%s \r\n", err)
		return err
	}
	return nil
}

// Update 修改DevopsApp对象
func (e *DevopsApp) Update(c *dto.DevopsAppUpdateReq, p *actions.DataPermission) error {
	var err error
	var data = models.DevopsApp{}
	e.Orm.Scopes(
		actions.Permission(data.TableName(), p),
	).First(&data, c.GetId())
	c.Generate(&data)

	db := e.Orm.Save(&data)
	if err = db.Error; err != nil {
		e.Log.Errorf("DevopsAppService Save error:%s \r\n", err)
		return err
	}
	if db.RowsAffected == 0 {
		return errors.New("无权更新该数据")
	}
	return nil
}

// Remove 删除DevopsApp
func (e *DevopsApp) Remove(d *dto.DevopsAppDeleteReq, p *actions.DataPermission) error {
	var data models.DevopsApp

	db := e.Orm.Model(&data).
		Scopes(
			actions.Permission(data.TableName(), p),
		).Delete(&data, d.GetId())
	if err := db.Error; err != nil {
		e.Log.Errorf("Service RemoveDevopsApp error:%s \r\n", err)
		return err
	}
	if db.RowsAffected == 0 {
		return errors.New("无权删除该数据")
	}
	return nil
}
