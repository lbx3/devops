package models

import (
	_ "time"

	"go-admin/common/models"
)

type DevopsProject struct {
	models.Model
	ProjectId int `json:"project_id" gorm:"primaryKey;autoIncrement;comment:主键编码"`

	ProjectName   string `json:"projectName" gorm:"type:varchar(128);comment:项目名"`
	GitGroup      string `json:"gitGroup" gorm:"type:varchar(128);comment:GIT组"`
	HarborProject string `json:"harborProject" gorm:"type:varchar(128);comment:制品库"`
	DeptId        string `json:"deptId" gorm:"type:bigint;comment:部门ID"`
	Owner         string `json:"owner" gorm:"type:varchar(64);comment:项目负责人"`
	JiraStory     string `json:"jiraStory" gorm:"type:varchar(128);comment:JIRA故事"`
	Desc          string `json:"desc" gorm:"type:text;comment:项目描述"`
	Alias         string `json:"alias" gorm:"type:varchar(64);comment:别名"`
	models.ModelTime
	models.ControlBy
}

func (DevopsProject) TableName() string {
	return "devops_project"
}

func (e *DevopsProject) Generate() models.ActiveRecord {
	o := *e
	return &o
}

func (e *DevopsProject) GetId() interface{} {
	return e.ProjectId
}
