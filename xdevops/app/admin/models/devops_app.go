package models

import (
	_ "time"

	"go-admin/common/models"
)

type DevopsApp struct {
	models.Model
	AppId int `json:"project_id" gorm:"primaryKey;autoIncrement;comment:主键编码"`

	AppName     string `json:"appName" gorm:"type:varchar(128);comment:应用名"`
	HarborImage string `json:"harborImage" gorm:"type:varchar(128);comment:制品"`
	GitRepo     string `json:"gitRepo" gorm:"type:varchar(128);comment:GIT仓库"`
	ProjectId   string `json:"projectId" gorm:"type:bigint;comment:项目ID"`
	Alias       string `json:"alias" gorm:"type:varchar(64);comment:别名"`
	Desc        string `json:"desc" gorm:"type:text;comment:描述"`
	Component   string `json:"component" gorm:"type:varchar(64);comment:Vela组件"`
	Env         string `json:"env" gorm:"type:varchar(64);comment:环境"`
	ExposeType  string `json:"exposeType" gorm:"type:enum('ClusterIP','NodePort','LoadBalancer');comment:暴露方式"`
	models.ModelTime
	models.ControlBy
}

func (DevopsApp) TableName() string {
	return "devops_app"
}

func (e *DevopsApp) Generate() models.ActiveRecord {
	o := *e
	return &o
}

func (e *DevopsApp) GetId() interface{} {
	return e.AppId
}
