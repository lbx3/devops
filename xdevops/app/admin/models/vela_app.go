package models

import "time"

type JSONData struct {
	Applications []Applications `json:"applications"`
}

type Labels struct {
	AdditionalProp1 string `json:"additionalProp1"`
	AdditionalProp2 string `json:"additionalProp2"`
	AdditionalProp3 string `json:"additionalProp3"`
}
type Owner struct {
	Alias string `json:"alias"`
	Name  string `json:"name"`
}
type Project struct {
	Alias       string    `json:"alias"`
	CreateTime  time.Time `json:"createTime"`
	Description string    `json:"description"`
	Name        string    `json:"name"`
	Namespace   string    `json:"namespace"`
	Owner       Owner     `json:"owner"`
	UpdateTime  time.Time `json:"updateTime"`
}
type Applications struct {
	Alias       string    `json:"alias"`
	CreateTime  time.Time `json:"createTime"`
	Description string    `json:"description"`
	Icon        string    `json:"icon"`
	Labels      Labels    `json:"labels"`
	Name        string    `json:"name"`
	Project     Project   `json:"project"`
	ReadOnly    bool      `json:"readOnly"`
	UpdateTime  time.Time `json:"updateTime"`
}
