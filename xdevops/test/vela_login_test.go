package test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
	"time"
)

type Login struct {
	User         User   `json:"user"`
	AccessToken  string `json:"accessToken"`
	RefreshToken string `json:"refreshToken"`
}
type User struct {
	CreateTime    time.Time `json:"createTime"`
	LastLoginTime time.Time `json:"lastLoginTime"`
	Name          string    `json:"name"`
	Email         string    `json:"email"`
	Disabled      bool      `json:"disabled"`
}

func TestVelaLogin(t *testing.T) {
	url := "https://vela.lbxdrugs.com/api/v1/auth/login"
	data := []byte(`{"username":"xupengjun","password":"liwanli1985"}`)
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Add("Content-Type", "application/json")
	client := &http.Client{Timeout: time.Second * 10}
	res, _ := client.Do(req)

	defer res.Body.Close()

	body, _ := ioutil.ReadAll(res.Body)

	resp := Login{}
	if err := json.Unmarshal(body, &resp); err != nil {
		fmt.Println(500, err.Error(), "反序列化失败")
		return
	}
	fmt.Println(resp.AccessToken)
}
