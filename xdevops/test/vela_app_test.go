package test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
	"time"
)

func TestVelaApi(t *testing.T) {
	url := "https://vela.lbxdrugs.com/api/v1/applications"
	token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Inh1cGVuZ2p1biIsImdyYW50VHlwZSI6ImFjY2VzcyIsImV4cCI6MTY4NjgwMTI2NywiaXNzIjoidmVsYS1pc3N1ZXIiLCJuYmYiOjE2ODY3OTc2Njd9.5Trn0cmcFJb8kxMIAmvtQLPVinXZW2hKD2Us9i77aqM"
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
	client := &http.Client{Timeout: time.Second * 10}
	res, err := client.Do(req)
	if err != nil {
		fmt.Printf("请求失败，错误信息：%v\n", err)
		return
	}
	defer res.Body.Close()

	fmt.Println("响应状态码：", res.StatusCode)
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Printf("读取响应内容失败，错误信息：%v\n", err)
		return
	}

	fmt.Println("响应内容：", string(body))
}
